<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Submit demonstration</title>
	<link href="style.css" rel="stylesheet">	
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/redmond/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script src="./myscript.js"></script>
</head>
<body>
<div id="main">
<div id="first">
<h1>Fill your data, please:</h1>
<div id="info-block"></div>
<form id="form" method="post" action="">
    <div>
        <label for="name">
            <br><p>Enter your name:</p>
            <input id="name" name="name" type="text">
        </label>
        <label for="email">
            <br><p>Enter your email:</p>
            <input id="email" name="email" type="text">
        </label><br>
        <p>Choose your gender:</p>
        <select id="gender" name="gender">
            <option selected value="male">Male</option>
            <option value="female">Female</option>
        </select>
        <label id="field_male" style="display: none;">
            <br><p>Well, man, say your word:</p>
            <input id="input_male" name="field_male" type="text">
        </label>
        <label id="field_female" style="display: none;">
            <br><p>Well, woman, say your word:</p>
            <input id="input_female" name="field_female"  type="text">
        </label><br>
        <input type="button" value="Send" id="submit">
        <input type="reset" id="reset">
    </div>
</form>
<div id="dialog" title="Information"></div>
</div>
</div>
</body>
</html>