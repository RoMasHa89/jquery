$(document).ready(function(){
    var sel = $("#gender");
    sel.data("prev",sel.val());

    $("#dialog").dialog(
        {
            autoOpen: false,
            modal: true,
            width: "30em",
            show: "fade",
            hide: "fade",
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                }
            },
            open: function()
            {
                $(".ui-widget-overlay").hide().fadeIn(300);
            },
            close: function()
            {
                $(".ui-widget-overlay").fadeOut(300);
            }
        });

    $('#field_'+sel.val()).show();

    sel.change(function(data){
        var jqThis = $(this);
        $('#field_'+jqThis.data("prev")).fadeOut(250, function() {
            $('#field_'+jqThis.val()).fadeIn(250);
        });
        // Make sure the previous value is updated
        jqThis.data("prev",jqThis.val());
    });

    $('#reset').click(function(){
        $('#info-block').empty();
    })

    $('#submit').click(function(){

        var key = true;

        $('#info-block').empty();

        if ( $("#name").val() == 0) {
            key = false;
            $( "#info-block" ).append( "<p>Enter <span>name</span>, please!</p><hr>" );
        }

        if ( $("#email").val() ) {
            if( !validateEmail( $("#email").val() ) ) {
                key = false;
                if ($( "#info-block" ).is(':empty')) {
                    $( "#info-block" ).append( "<p>Enter valid e-mail, please!</p><hr>" );
                } else $( "#info-block" ).find("span").append( " and valid e-mail" );
            }
        } else {
            key = false;
            if ($( "#info-block" ).is(':empty')) {
                $( "#info-block" ).append( "<p>Enter e-mail, please!</p><hr>" );
            } else $( "#info-block" ).find("span").append( " and e-mail" );

        }

        if ( key ) {
            var data = [];

            $( "#submit" ).val( "Sending E-mail..." );
            data = $("#form").serializeArray();
            $.post("send.php", data,  function(response) {
                $( "#dialog" ).dialog({
                    title: "Attention!",
                    open: function () {
                        $(this).html(response);
                    }
                });
                $( "#dialog" ).dialog("open");
                $( "#submit" ).val( "Send" );
            });
            return 1;

        } else {
            $( "#info-block" ).show('slow')
            return 0;
        }

    });

});

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}