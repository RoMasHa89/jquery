<?php
// Here we get all the information from the fields sent over by the form.
$name = $_POST['name'];
$email = $_POST['email'];
$gender =$_POST['gender'];
if ($gender == 'male') {
    $data =  $_POST['field_male'];
} else {
    $data = $_POST['field_female'];
}

$message = "
<html>
    <head>
        <style>
            td, th {
              border: 1px solid #999;
              padding: 0.5rem;
            }
        </style>
        <title>Your form</title>
    </head>
    <body>
        <h1>Your data below:</h1><hr>
        <table>
            <tr>
                <th>Name:</th>
                <td>".$name."</td>
            </tr>
            <tr>
                <th>Email:</th>
                <td>".$email."</td>
            </tr>
            <tr>
                <th>Gender:</th>
                <td>".$gender."</td>
            </tr>
                <th>Your text:</th>
                <td>" . $data . "</td>
            </tr>
        </table>
    </body>
</html>
";

$subject = 'Your data!';

$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= 'From: ' . $name .  '<' . $email . '>\r\n';

if (mail($email, $subject, $message, $headers)) {
    die ("Your email was sent!"); // success message
} else {
    die ("There is some problem with send_mail!");
}

?>